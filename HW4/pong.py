#authors: Katrina Liebsch and Kilian Belz

import simpleGraphics as sg
import time
import random as rnd
import sys
from math import sqrt
from numpy import mean, sign



###############################################################################
# Class Definitions
###############################################################################

###############################################################################
# vector class to aid in game's physics calculations
class vector():
	def __init__(self, x=0, y=0):
		self.x = x
		self.y = y

	def __add__(self,other):
		return vector(self.x + other.x, self.y + other.y)

	def __sub__(self,other):
		return vector(self.x - other.x, self.y - other.y)

	def __mul__(self,other):
		return vector(self.x * other, self.y * other)

	def __div__(self,other):
		return vector(self.x / other, self.y / other)

	def __repr__(self):
		return "<" + str(self.x) + ", " + str(self.y) + ">"

	def magnitude(self):
		return sqrt(self.x**2 + self.y**2)

	def unit_vec(self):
		return vector(self.x / self.magnitude(), self.y / self.magnitude())

###############################################################################


###############################################################################
# handles the balls characteristics
class Ball():
	def __init__(self, GUI, position=vector(0,0), velocity=vector(100,0), radius=5):
		self.position = position
		self.velocity = velocity
		self.radius = radius
		self.ball_drw = sg.Circle(sg.Point(self.position.x, self.position.y), radius)
		self.ball_drw.draw(GUI.win)

	def set_position(self, vector):
		curr_position = self.position
		self.position = vector
		dx = self.position.x - curr_position.x
		dy = self.position.y - curr_position.y
		self.ball_drw.move(dx, dy)

	def move(self):
		self.position = self.position + self.velocity*0.01
		self.ball_drw.move(self.velocity.x*0.01, self.velocity.y*0.01)

	def set_v0(self):
		random_f = rnd.random() - 0.5
		if(random_f >= 0):
			x_factor = random_f + 0.5
		else:
			x_factor = random_f - 0.5

		if(rnd.random()-0.5 >= 0):
			y_factor = sqrt(1 - x_factor**2)
		else:
			y_factor = -sqrt(1 - x_factor**2)

		self.velocity = vector(x_factor, y_factor)*100

	def turbospeed(self):
		self.velocity = self.velocity*1.25

	def transform_speed(self, on=True):
		if on:
			self.velocity = self.velocity * 3.0
		else:
			self.velocity = self.velocity / 3.0

	def gravity(self, grav_constant=1):
		self.velocity = vector(self.velocity.x, self.velocity.y-grav_constant)		

	def reflect(self, factor):
		self.velocity = vector(self.velocity.x*factor.x, self.velocity.y*factor.y)

	def adv_reflect(self, factor, paddle):
		if (factor.x==-1) &((self.velocity.y*(self.position.y-paddle.position.y))<0):
			if self.velocity.y>0:
				self.velocity = vector(-self.velocity.x, (-self.velocity.y-self.velocity.y*((self.position.y-paddle.position.y)/20)))
				print 'hit'
				print self.velocity
			elif self.velocity.y<0:
				self.velocity = vector(-self.velocity.x, (-self.velocity.y-self.velocity.y*((self.position.y-paddle.position.y)/20)))
		else:		
			self.velocity = vector(self.velocity.x*factor.x, self.velocity.y*factor.y)		

	def __repr__(self):
		return "Position: " + str(self.position) + "\nVelocity: " + str(self.velocity)
###############################################################################


###############################################################################
# handles the paddle characteristics
class Paddle():
	def __init__(self, GUI, position=vector(-100,0)):
		self.position = position
		self.paddle_drw = sg.Rectangle(sg.Point(position.x-5,position.y-20), sg.Point(position.x+5,position.y+20))
		self.paddle_drw.draw(GUI.win)
		self.speed = 10

	def set_position(self, vector):
		curr_position = self.position
		self.position = vector
		dx = self.position.x - curr_position.x
		dy = self.position.y - curr_position.y
		self.paddle_drw.move(dx,dy)

	def move(self, direction):
		if (direction == 1) & (self.position.y < 80):
			self.position = self.position + vector(0,self.speed)
			self.paddle_drw.move(0,self.speed)

		if (direction == -1) & (self.position.y > -80):
			self.position = self.position + vector(0,-self.speed)
			self.paddle_drw.move(0,-self.speed)

	def transform_speed(self, on=True):
		if on:
			self.speed = 20
		else:
			self.speed = 10

	def __repr__(self):
		return "Position: " + str(self.position)
###############################################################################


###############################################################################
# handles the goal characteristics
class Goal():
	def __init__(self, GUI, p1=vector(-10,-10), p2=vector(10,10)):
		self.p1 = p1
		self.p2 = p2
		self.goal_drw = sg.Rectangle(sg.Point(p1.x,p1.y), sg.Point(p2.x,p2.y))
		self.goal_drw.setFill('gray')
		self.goal_drw.setOutline('gray')
		self.goal_drw.draw(GUI.win)
###############################################################################


###############################################################################
# handles the border characteristics
class Border():
	def __init__(self, GUI, p1=vector(-10,-10), p2=vector(10,10)):
		self.p1 = p1
		self.p2 = p2
		self.border_drw = sg.Rectangle(sg.Point(p1.x,p1.y), sg.Point(p2.x,p2.y))
		self.border_drw.setFill('black')
		self.border_drw.draw(GUI.win)
###############################################################################


###############################################################################
# handles the Portal characteristics
class Portal():
	def __init__(self, GUI, p1=vector(-10,-10), p2=vector(10,10)):
		self.p1 = p1
		self.p2 = p2
		self.portal_drw = sg.Rectangle(sg.Point(p1.x,p1.y), sg.Point(p2.x,p2.y))
		self.portal_drw.setFill('blue')
		self.portal_drw.draw(GUI.win)
		self.twin = None

	def pair_portals(self, twin_portal):
		self.twin = twin_portal
		twin_portal.twin = self

	def ball_contact(self, ball):
		if (ball.position.x - self.p1.x)*(ball.position.x - self.p2.x) < 0:
			if (ball.position.y - self.p1.y)*(ball.position.y - self.p2.y) < 0:
				return True
		else:
			return False

	def teleport(self, ball):
		twin_center = vector(mean([self.twin.p1.x, self.twin.p2.x]), mean([self.twin.p1.y, self.twin.p2.y]))
		twin_size = vector(abs(self.twin.p1.x - self.twin.p2.x), abs(self.twin.p1.y - self.twin.p2.y))

		if ball.velocity.x < 0:
			if ball.velocity.y > 0:
				ball.set_position(vector((twin_center.x - twin_size.x / 2.0), (twin_center.y + twin_size.y / 2.0)))
				print ball
			else:
				ball.set_position(vector((twin_center.x - twin_size.x / 2.0), (twin_center.y - twin_size.y / 2.0)))
				print ball
		else:
			if ball.velocity.y > 0:
				ball.set_position(vector((twin_center.x + twin_size.x / 2.0), (twin_center.y + twin_size.y / 2.0)))
				print ball
			else:
				ball.set_position(vector((twin_center.x + twin_size.x / 2.0), (twin_center.y - twin_size.y / 2.0)))
				print ball

	def destruct(self):
		self.portal_drw.undraw()
###############################################################################


###############################################################################
# handles the brick characteristics
class Brick():
	def __init__(self, GUI, p1=vector(-10,-10), p2=vector(10,10)):
		self.p1 = p1
		self.p2 = p2
		self.brick_drw = sg.Rectangle(sg.Point(p1.x,p1.y), sg.Point(p2.x,p2.y))
		self.brick_drw.setFill('brown')
		self.brick_drw.draw(GUI.win)

		self.rebound_fact = vector(0,0)

	def detect_hit(self, ball):
		vel_dir = vector(sign(ball.position.x), sign(ball.position.y))
		brick_cent = vector(mean([self.p1.x, self.p2.x]), mean([self.p1.y, self.p2.y]))
		brick_size = vector(abs(self.p1.x - self.p2.x) / 2.0, abs(self.p1.y - self.p2.y) / 2.0)

		if abs(ball.position.x - brick_cent.x) <= brick_size.x + ball.radius:
			if abs(ball.position.y - brick_cent.y) <= brick_size.y:
				self.rebound_fact = vector(-1,1)
				return True
		if abs(ball.position.x - brick_cent.x) <= brick_size.x:
			if abs(ball.position.y - brick_cent.y) <= brick_size.y + ball.radius:
				self.rebound_fact = vector(1,-1)
				return True
		if sqrt((abs(ball.position.x - brick_cent.x) - brick_size.x)**2 + (abs(ball.position.y - brick_cent.y) - brick_size.y)**2) < ball.radius:
			self.rebound_fact = vector(-1,-1)
			return True

	def break_brick(self, ball):
		self.brick_drw.undraw()
		ball.reflect(self.rebound_fact)

	def destruct(self):
		self.brick_drw.undraw()
###############################################################################


###############################################################################
# handles the creature characteristics
class Creature():
	def __init__(self, GUI, position=vector(0,0), size=vector(20,20)):
		self.position = position
		self.size = size
		self.p1 = vector(self.position.x - size.x / 2.0, self.position.y - size.y / 2.0)
		self.p2 = vector(self.position.x + size.x / 2.0, self.position.y + size.y / 2.0)

		self.creature_drw = sg.Rectangle(sg.Point(self.p1.x, self.p1.y), sg.Point(self.p2.x, self.p2.y))
		self.creature_drw.setFill('green')
		self.creature_drw.setOutline('green')
		self.creature_drw.draw(GUI.win)

		self.linger = 0

	def set_position(self, vector):
		curr_position = self.position
		self.position = vector
		dx = self.position.x - curr_position.x
		dy = self.position.y - curr_position.y
		self.creature_drw.move(dx,dy)

	def detect_hit(self, ball):
		if abs(ball.position.x - self.position.x) <= self.size.x/2.0 + ball.radius:
			if abs(ball.position.y - self.position.y) <= self.size.y/2.0:
				self.rebound_fact = vector(-1,1)
				return True
		if abs(ball.position.x - self.position.x) <= self.size.x/2.0:
			if abs(ball.position.y - self.position.y) <= self.size.y/2.0 + ball.radius:
				self.rebound_fact = vector(1,-1)
				return True
		if sqrt((abs(ball.position.x - self.position.x) - self.size.x/2.0)**2 + (abs(ball.position.y - self.position.y) - self.size.y/2.0)**2) < ball.radius:
			self.rebound_fact = vector(-1,-1)
			return True

	def ball_rebound(self, ball):
		ball.reflect(self.rebound_fact)

	def check_lingering(self):
		if self.linger > 25:
			self.linger = 0
			return True
		else:
			self.linger += 1

	def wander(self):
		wander_to = self.position + vector(rnd.randint(-10,10), rnd.randint(-10,10))
		if (abs(self.position.x) + self.size.x/2.0 < 140) & (abs(self.position.y) + self.size.y/2.0 < 95):
			self.set_position(wander_to)

	def destruct(self):
		self.creature_drw.undraw()
		
###############################################################################


###############################################################################
#handles the Chaos Cloud characteristics
class Chaos():
	def __init__(self,GUI, p1=vector(-10,-10)):
		self.p1 = p1
		self.p2 =  vector(self.p1.x+40, self.p1.y+40)
		self.chaos_drw = sg.Rectangle(sg.Point(self.p1.x,self.p1.y), sg.Point(self.p2.x,self.p2.y))
		# self.chaos_drw.setFill('purple')
		self.chaos_drw.draw(GUI.win)

	def ball_contact(self, ball):
		if (ball.position.x - self.p1.x)*(ball.position.x - self.p2.x) < 0:
			if (ball.position.y - self.p1.y)*(ball.position.y - self.p2.y) < 0:
				return True
		else:
			return False

	def divert(self, ball):
		random_f = rnd.random() - 0.5
		if(random_f >= 0):
			x_factor = random_f + 0.5
		else:
			x_factor = random_f - 0.5

		if(rnd.random()-0.5 >= 0):
			y_factor = sqrt(1 - x_factor**2)
		else:
			y_factor = -sqrt(1 - x_factor**2)

		if ball.position.x<(self.p2.x-2):
			ball.set_position(vector((self.p2.x+1), ball.position.y))
		elif ball.position.x>(self.p1.x+2):
			ball.set_position(vector((self.p1.x-1), ball.position.y))

		ball.velocity = vector(x_factor, y_factor)*100

	def destruct(self):
		self.chaos_drw.undraw()
###############################################################################


###############################################################################
# handles the restart button
class RestartButton():
	def __init__(self, GUI, p=vector(0,0)):
		self.p = p
		self.gui = GUI
		self.rstrt_butt_drw = sg.Rectangle(sg.Point(self.p.x-15, self.p.y-5), sg.Point(self.p.x+15, self.p.y+5))
		self.rstrt_butt_txt = sg.Text(sg.Point(self.p.x, self.p.y), "Restart")
		self.rstrt_butt_drw.draw(GUI.win)
		self.rstrt_butt_txt.draw(GUI.win)

	def clicked_restart(self):
		location = self.gui.win.checkMouse()

		if location is not None:
			return self.rstrt_butt_drw.inRegion(location)
		else:
			return False
###############################################################################


###############################################################################
# handles the quit button
class QuitButton():
	def __init__(self, GUI, p=vector(0,0)):
		self.p = p
		self.gui = GUI
		self.quit_butt_drw = sg.Rectangle(sg.Point(self.p.x-15, self.p.y-5), sg.Point(self.p.x+15, self.p.y+5))
		self.quit_butt_txt = sg.Text(sg.Point(self.p.x, self.p.y), "Quit")
		self.quit_butt_drw.draw(GUI.win)
		self.quit_butt_txt.draw(GUI.win)

	def clicked_quit(self):
		location = self.gui.win.checkMouse()
		
		self.gui.win.mouseX = None
		self.gui.win.mouseY = None

		if location is not None:
			return self.quit_butt_drw.inRegion(location)
		else:
			return False
###############################################################################


###############################################################################
# handles the score
class ScoreDisplay():
	def __init__(self, GUI, p=vector(0,0)):
		self.p = p
		self.score = 0
		self.score_drw = sg.Text(sg.Point(p.x,p.y), str(self.score))
		self.score_drw.setSize(20)
		self.score_drw.draw(GUI.win)

	def increment_score(self):
		self.score += 1
		self.score_drw.setText(str(self.score))

	def reset_score(self):
		self.score_drw.setText("0")
###############################################################################


###############################################################################
# handles the game graphics
class GUI():
	def __init__(self):
		self.win = sg.GraphWin(width=640, height=500)
		self.win.setCoords(-160,-140,160,110)

	def press_s(self):
		return self.win.check_s()

	def press_w(self):
		return self.win.check_w()

	def press_up(self):
		return self.win.check_up()

	def press_down(self):
		return self.win.check_down()

	def quit(self):
		self.win.close()
###############################################################################


###############################################################################
# handles the gameplay
class pong():
	def __init__(self):
		self.graphics = GUI()
		self.paddlehit_count = 0
		self.adv_feat_on = False
		self.adv_feat_num = 0 
		self.gravity_on = False

		self.paddle1 = Paddle(self.graphics, vector(-145,0))
		self.paddle2 = Paddle(self.graphics, vector(145,0))
		self.goal1 = Goal(self.graphics, vector(-160,-100), vector(-150,100))
		self.goal2 = Goal(self.graphics, vector(150,-100), vector(160,100))
		self.border_top = Border(self.graphics, vector(-149,95), vector(149,100))
		self.border_bot = Border(self.graphics, vector(-149,-95), vector(149,-100))

		self.main_ball = Ball(self.graphics)
		self.main_ball.set_v0()

		self.score1 = ScoreDisplay(self.graphics, vector(-120,-120))
		self.score2 = ScoreDisplay(self.graphics, vector(120,-120))
		self.restart_button = RestartButton(self.graphics, vector(-50,-120))
		self.quit_button = QuitButton(self.graphics, vector(50,-120))

		self.events = [self.restart_button.clicked_restart,
			self.quit_button.clicked_quit,
			self.graphics.press_s,
			self.graphics.press_w,
			self.graphics.press_up,
			self.graphics.press_down,
			self.topwallhit, self.bottomwallhit, self.leftpaddlehit, self.rightpaddlehit,
			self.scoreleft, self.scoreright, self.check_paddlehits,
			self.init_adv_feat]

		self.responses = [self.restart, sys.exit, 
			lambda: self.paddle1.move(-1),
			lambda: self.paddle1.move(1),
			lambda: self.paddle2.move(1),
			lambda: self.paddle2.move(-1),
			lambda: self.main_ball.reflect(vector(1,-1)),
			lambda: self.main_ball.reflect(vector(1,-1)), 
			lambda: self.main_ball.adv_reflect(vector(-1,1),self.paddle1), 
			lambda: self.main_ball.adv_reflect(vector(-1,1),self.paddle2),
			lambda: self.score(1),
			lambda: self.score(2),
			self.main_ball.turbospeed,
			self.adv_feat_exec]

	# events functions
	def topwallhit(self):
		return self.main_ball.position.y > (95 - self.main_ball.radius)

	def bottomwallhit(self):
		return self.main_ball.position.y < (-95 + self.main_ball.radius)

	def leftpaddlehit(self):
		if (self.main_ball.velocity.x < 0):
			if (self.main_ball.position.x < (-140 + self.main_ball.radius)) & (self.main_ball.position.x > -142):
				if (abs(self.main_ball.position.y - self.paddle1.position.y) < 20):
					self.paddlehit_count += 1
					print self.paddlehit_count
					return True

	def rightpaddlehit(self):
		if (self.main_ball.velocity.x > 0):
			if (self.main_ball.position.x > (140 - self.main_ball.radius)) & (self.main_ball.position.x < 142):
				if (abs(self.main_ball.position.y - self.paddle2.position.y) < 20):
					self.paddlehit_count += 1
					print self.paddlehit_count
					return True

	def scoreleft(self):
		return self.main_ball.position.x < (-150 - self.main_ball.radius)

	def scoreright(self):
		return self.main_ball.position.x > (150 + self.main_ball.radius)

	def check_paddlehits(self):
		if self.paddlehit_count >= 5:
			self.paddlehit_count = 0
			return True
		else:
			return False

	def gravity(self):
		return self.gravity_on

	def init_adv_feat(self):
		if self.adv_feat_on:
			self.adv_feat_time += 1
			if self.adv_feat_time > 2000:
				self.delete_adv_feat()
				print "Feature timeout."
				self.adv_feat_on = False
		else:
			if rnd.random() < 0.001:
				self.adv_feat_on = True
				self.adv_feat_time = 0
				self.adv_feat_num = 7
				self.create_adv_feat()
				print "Feature " + str(self.adv_feat_num) + " turned on."

		return self.adv_feat_on



	# response functions
	def score(self, player):
		self.paddlehit_count = 0

		if(player == 1):
			self.score2.increment_score()
		if(player == 2):
			self.score1.increment_score()

		self.main_ball.set_position(vector(0,0))
		self.main_ball.set_v0()
		self.paddle1.set_position(vector(-145,0))
		self.paddle2.set_position(vector(145,0))
		time.sleep(2)

		self.delete_adv_feat()
		self.adv_feat_on = False

	def restart(self):
		self.paddlehit_count = 0
		
		self.score1.reset_score()
		self.score2.reset_score()
		self.main_ball.set_position(vector(0,0))
		self.main_ball.set_v0()
		self.paddle1.set_position(vector(-145,0))
		self.paddle2.set_position(vector(145,0))
		time.sleep(2)

		self.delete_adv_feat()
		self.adv_feat_on = False

	def brickhit_resp(self):
		self.brick.break_brick(self.main_ball)
		del self.brick

		self.adv_feat_on = False

	def adv_feat_exec(self):
		self.adv_events = [lambda: self.portal1.ball_contact(self.main_ball),
			lambda: self.portal2.ball_contact(self.main_ball),
			lambda: self.brick.detect_hit(self.main_ball),
			lambda: self.chaoscloud.ball_contact(self.main_ball),
			lambda: self.creature.detect_hit(self.main_ball),
			lambda: self.creature.check_lingering(),
			self.gravity]

		self.adv_responses = [lambda: self.portal1.teleport(self.main_ball),
			lambda: self.portal2.teleport(self.main_ball),
			self.brickhit_resp,
			lambda: self.chaoscloud.divert(self.main_ball),
			lambda: self.creature.ball_rebound(self.main_ball),
			lambda: self.creature.wander(),
			self.main_ball.gravity]

		if self.adv_feat_num == 4:
			if self.adv_events[0](): self.adv_responses[0]()
			elif self.adv_events[1](): self.adv_responses[1]()
		elif self.adv_feat_num == 5:
			if self.adv_events[2](): self.adv_responses[2]()
		elif self.adv_feat_num == 6:
			if self.adv_events[3](): self.adv_responses[3]()
		elif self.adv_feat_num == 7:
			if self.adv_events[4](): self.adv_responses[4]()
			elif self.adv_events[5](): self.adv_responses[5]()
		elif self.adv_feat_num == 8:
			if self.adv_events[6](): self.adv_responses[6]()


	# miscellaneous functions
	def create_adv_feat(self):
		if self.adv_feat_num == 1:
			self.main_ball.transform_speed(on=True)
		elif self.adv_feat_num == 2:
			self.paddle1.transform_speed(on=True)
			self.paddle2.transform_speed(on=True)
		elif self.adv_feat_num == 4:
			port1_rand = vector(rnd.randint(-100,-5), rnd.randint(-80,80))
			port2_rand = vector(rnd.randint(5,100), rnd.randint(-80,80))
			self.portal1 = Portal(self.graphics, port1_rand, port1_rand + vector(rnd.randint(-10,10), rnd.randint(-10,10)))
			self.portal2 = Portal(self.graphics, port2_rand, port2_rand + vector(rnd.randint(-10,10), rnd.randint(-10,10)))
			self.portal1.pair_portals(self.portal2),
		elif self.adv_feat_num == 5:
			brick_rand = vector(rnd.randint(-120,120), rnd.randint(-70,70))
			self.brick = Brick(self.graphics, brick_rand, brick_rand + vector(rnd.randint(-20,20), rnd.randint(-20,20)))
		elif self.adv_feat_num == 6:
			self.chaoscloud = Chaos(self.graphics, vector(rnd.randint(-120,120), rnd.randint(-70,70)))
		elif self.adv_feat_num == 7:
			self.creature = Creature(self.graphics, vector(rnd.randint(-120,120), rnd.randint(-70,70)))
		elif self.adv_feat_num == 8:
			self.gravity_on = True

	def delete_adv_feat(self):
		if self.adv_feat_num == 1:
			self.main_ball.transform_speed(on=False)
		elif self.adv_feat_num == 2:
			self.paddle1.transform_speed(on=False)
			self.paddle2.transform_speed(on=False)
		elif self.adv_feat_num == 4:
			for portal in [self.portal1, self.portal2]:
				portal.destruct()
				del portal
		elif self.adv_feat_num == 5:
			self.brick.destruct()
			del self.brick
		elif self.adv_feat_num == 6:
			self.chaoscloud.destruct()
			del self.chaoscloud
		elif self.adv_feat_num == 7:
			self.creature.destruct()
			del self.creature
		elif self.adv_feat_num == 8:
			self.gravity_on = False
###############################################################################



###############################################################################
# Main Function
###############################################################################

pong_game = pong()

time.sleep(1)

while True:
	for event, response in zip(pong_game.events, pong_game.responses):
		if event():
			response()
	pong_game.main_ball.move()
	time.sleep(0.001)




		

