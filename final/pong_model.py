# authors: Kilian Belz and Katrina Liebsch

import time, sys
import random as rnd
from vector_class import vector
from pong_view import view
from pong_controller import controller
from math import sqrt
from numpy import mean, sign

###############################################################################
# Class Definitions
###############################################################################


###############################################################################
# handles the balls characteristics
class Ball():
	def __init__(self, position=vector(0,0), velocity=vector(100,0), radius=5):
		self.position = position
		self.velocity = velocity
		self.radius = radius

	def set_position(self, vector):
		self.position = vector

	def move(self):
		self.position = self.position + self.velocity*0.01

	def set_v0(self):
		random_f = rnd.random() - 0.5
		if(random_f >= 0):
			x_factor = random_f + 0.5
		else:
			x_factor = random_f - 0.5

		if(rnd.random()-0.5 >= 0):
			y_factor = sqrt(1 - x_factor**2)
		else:
			y_factor = -sqrt(1 - x_factor**2)

		self.velocity = vector(x_factor, y_factor)*200

	def turbospeed(self):
		self.velocity = self.velocity*1.25

	def transform_speed(self, on=True):
		if on:
			self.velocity = self.velocity * 3.0
		else:
			self.velocity = self.velocity / 3.0

	def gravity(self, grav_constant=1):
		self.velocity = vector(self.velocity.x, self.velocity.y-grav_constant)		

	def reflect(self, factor):
		self.velocity = vector(self.velocity.x*factor.x, self.velocity.y*factor.y)

	def adv_reflect(self, factor, paddle):
		if (factor.x==-1) & ((self.velocity.y*(self.position.y-paddle.position.y))<0):
			if self.velocity.y>0:
				self.velocity = vector(-self.velocity.x, (-self.velocity.y-self.velocity.y*((self.position.y-paddle.position.y)/20)))
				print 'hit'
				print self.velocity
			elif self.velocity.y<0:
				self.velocity = vector(-self.velocity.x, (-self.velocity.y-self.velocity.y*((self.position.y-paddle.position.y)/20)))
		else:		
			self.velocity = vector(self.velocity.x*factor.x, self.velocity.y*factor.y)		

	def __repr__(self):
		return "Position: " + str(self.position) + "\nVelocity: " + str(self.velocity)
###############################################################################


###############################################################################
# handles the paddle characteristics
class Paddle():
	def __init__(self, border1, border2, position=vector(-100,0)):
		self.position = position
		self.size = vector(10,40)
		self.speed = 10
		self.top_limit = border1.p1.y
		self.bottom_limit = border2.p2.y

	def set_position(self, vector):
		self.position = vector

	def move(self, direction):
		if direction == 1:
			self.position = self.position + vector(0,self.speed)
			if self.position.y + self.size.y / 2.0 > self.top_limit:
				self.position.y = self.top_limit - self.size.y / 2.0

		if direction == -1:
			self.position = self.position + vector(0,-self.speed)
			if self.position.y - self.size.y / 2.0 < self.bottom_limit:
				self.position.y = self.bottom_limit + self.size.y / 2.0

	def transform_speed(self, on=True):
		if on:
			self.speed = 20
		else:
			self.speed = 10

	def __repr__(self):
		return "Position: " + str(self.position)
###############################################################################


###############################################################################
# handles the goal characteristics
class Goal():
	def __init__(self, p1=vector(-10,-10), p2=vector(10,10)):
		if p1.x > p2.x:
			self.p1 = p2
			self.p2 = p1
		else:
			self.p1 = p1
			self.p2 = p2
###############################################################################


###############################################################################
# handles the border characteristics
class Border():
	def __init__(self, p1=vector(-10,-10), p2=vector(10,10)):
		if p1.y > p2.y:
			self.p1 = p2
			self.p2 = p1
		else:
			self.p1 = p1
			self.p2 = p2
###############################################################################


###############################################################################
# handles the Portal characteristics
class Portal():
	def __init__(self, p1=vector(-10,-10), p2=vector(10,10)):
		self.p1 = p1
		self.p2 = p2
		self.twin = None

	def pair_portals(self, twin_portal):
		self.twin = twin_portal
		twin_portal.twin = self

	def ball_contact(self, ball):
		if (ball.position.x - self.p1.x)*(ball.position.x - self.p2.x) < 0:
			if (ball.position.y - self.p1.y)*(ball.position.y - self.p2.y) < 0:
				return True
		else:
			return False

	def teleport(self, ball):
		twin_center = vector(mean([self.twin.p1.x, self.twin.p2.x]), mean([self.twin.p1.y, self.twin.p2.y]))
		twin_size = vector(abs(self.twin.p1.x - self.twin.p2.x), abs(self.twin.p1.y - self.twin.p2.y))

		if ball.velocity.x < 0:
			if ball.velocity.y > 0:
				ball.set_position(vector((twin_center.x - twin_size.x / 2.0), (twin_center.y + twin_size.y / 2.0)))
				print ball
			else:
				ball.set_position(vector((twin_center.x - twin_size.x / 2.0), (twin_center.y - twin_size.y / 2.0)))
				print ball
		else:
			if ball.velocity.y > 0:
				ball.set_position(vector((twin_center.x + twin_size.x / 2.0), (twin_center.y + twin_size.y / 2.0)))
				print ball
			else:
				ball.set_position(vector((twin_center.x + twin_size.x / 2.0), (twin_center.y - twin_size.y / 2.0)))
				print ball
###############################################################################


###############################################################################
# handles the brick characteristics
class Brick():
	def __init__(self, p1=vector(-10,-10), p2=vector(10,10)):
		self.p1 = p1
		self.p2 = p2
		self.rebound_fact = vector(0,0)

	def detect_hit(self, ball):
		vel_dir = vector(sign(ball.position.x), sign(ball.position.y))
		brick_cent = vector(mean([self.p1.x, self.p2.x]), mean([self.p1.y, self.p2.y]))
		brick_size = vector(abs(self.p1.x - self.p2.x) / 2.0, abs(self.p1.y - self.p2.y) / 2.0)

		if abs(ball.position.x - brick_cent.x) <= brick_size.x + ball.radius:
			if abs(ball.position.y - brick_cent.y) <= brick_size.y:
				self.rebound_fact = vector(-1,1)
				return True
		if abs(ball.position.x - brick_cent.x) <= brick_size.x:
			if abs(ball.position.y - brick_cent.y) <= brick_size.y + ball.radius:
				self.rebound_fact = vector(1,-1)
				return True
		if sqrt((abs(ball.position.x - brick_cent.x) - brick_size.x)**2 + (abs(ball.position.y - brick_cent.y) - brick_size.y)**2) < ball.radius:
			self.rebound_fact = vector(-1,-1)
			return True

	def break_brick(self, ball):
		ball.reflect(self.rebound_fact)
###############################################################################


###############################################################################
# handles the creature characteristics
class Creature():
	def __init__(self, position=vector(0,0), size=vector(20,20)):
		self.position = position
		self.size = size
		self.p1 = vector(self.position.x - size.x / 2.0, self.position.y - size.y / 2.0)
		self.p2 = vector(self.position.x + size.x / 2.0, self.position.y + size.y / 2.0)

		self.linger = 0

	def set_position(self, new_position):
		self.position = new_position
		self.p1 = vector(self.position.x - self.size.x / 2.0, self.position.y - self.size.y / 2.0)
		self.p2 = vector(self.position.x + self.size.x / 2.0, self.position.y + self.size.y / 2.0)

	def detect_hit(self, ball):
		if abs(ball.position.x - self.position.x) <= self.size.x/2.0 + ball.radius:
			if abs(ball.position.y - self.position.y) <= self.size.y/2.0:
				self.rebound_fact = vector(-1,1)
				return True
		if abs(ball.position.x - self.position.x) <= self.size.x/2.0:
			if abs(ball.position.y - self.position.y) <= self.size.y/2.0 + ball.radius:
				self.rebound_fact = vector(1,-1)
				return True
		if sqrt((abs(ball.position.x - self.position.x) - self.size.x/2.0)**2 + (abs(ball.position.y - self.position.y) - self.size.y/2.0)**2) < ball.radius:
			self.rebound_fact = vector(-1,-1)
			return True

	def ball_rebound(self, ball):
		ball.reflect(self.rebound_fact)

	def check_lingering(self):
		if self.linger > 25:
			self.linger = 0
			return True
		else:
			self.linger += 1

	def wander(self):
		wander_to = self.position + vector(rnd.randint(-10,10), rnd.randint(-10,10))
		if (abs(wander_to.x) + self.size.x/2.0 < 140) & (abs(wander_to.y) + self.size.y/2.0 < 95):
			self.set_position(wander_to)
###############################################################################


###############################################################################
#handles the Chaos Cloud characteristics
class Chaos():
	def __init__(self, p1=vector(-10,-10)):
		self.p1 = p1
		self.p2 =  vector(self.p1.x+40, self.p1.y+40)

	def ball_contact(self, ball):
		if (ball.position.x - self.p1.x)*(ball.position.x - self.p2.x) < 0:
			if (ball.position.y - self.p1.y)*(ball.position.y - self.p2.y) < 0:
				return True
		else:
			return False

	def divert(self, ball):
		random_f = rnd.random() - 0.5
		if(random_f >= 0):
			x_factor = random_f + 0.5
		else:
			x_factor = random_f - 0.5

		if(rnd.random()-0.5 >= 0):
			y_factor = sqrt(1 - x_factor**2)
		else:
			y_factor = -sqrt(1 - x_factor**2)

		if ball.position.x<(self.p2.x-2):
			ball.set_position(vector((self.p2.x+1), ball.position.y))
		elif ball.position.x>(self.p1.x+2):
			ball.set_position(vector((self.p1.x-1), ball.position.y))

		ball.velocity = vector(x_factor, y_factor)*200
###############################################################################


###############################################################################
# handles the score
class Score():
	def __init__(self):
		self.score1 = 0
		self.score2 = 0
###############################################################################


###############################################################################
# handles visual object locations and states for view to use to display the objects
class pong_state():
	def __init__(self, pong):
		self.ball_radius = pong.main_ball.radius
		self.paddle1_size = pong.paddle1.size
		self.paddle2_size = pong.paddle2.size
		self.goal1_area = [pong.goal1.p1, pong.goal1.p2]
		self.goal2_area = [pong.goal2.p1, pong.goal2.p2]
		self.topborder_area = [pong.border_top.p1, pong.border_top.p2]
		self.bottomborder_area = [pong.border_bot.p1, pong.border_bot.p2]

		self.ball_position = pong.main_ball.position
		self.paddle1_position = pong.paddle1.position
		self.paddle2_position = pong.paddle2.position
		self.scores = [pong.game_score.score1, pong.game_score.score2]

	def update(self, pong):
		self.ball_position = pong.main_ball.position
		self.paddle1_position = pong.paddle1.position
		self.paddle2_position = pong.paddle2.position
		self.scores = [pong.game_score.score1, pong.game_score.score2]

		self.feature_info = [pong.state_vars['adv_feat_on'], pong.state_vars['adv_feat_num']]
		if pong.state_vars['adv_feat_on']:
			if pong.state_vars['adv_feat_num'] == 4:
				self.adv_position = [pong.portal1.p1, pong.portal1.p2, pong.portal2.p1, pong.portal2.p2]
			elif pong.state_vars['adv_feat_num'] == 5:
				self.adv_position = [pong.brick.p1, pong.brick.p2]
			elif pong.state_vars['adv_feat_num'] == 6:
				self.adv_position = [pong.chaoscloud.p1, pong.chaoscloud.p2]
			elif pong.state_vars['adv_feat_num'] == 7:
				self.adv_position = [pong.creature.p1, pong.creature.p2]
			else:
				self.adv_position = None
###############################################################################


###############################################################################
class pong():
	def __init__(self):
		self.state_vars = {'paddlehit_count': 0, 'adv_feat_on': False, 'adv_feat_num': 0, 'gravity_on': False}

		self.goal1 = Goal(vector(-160,-100), vector(-150,100))
		self.goal2 = Goal(vector(150,-100), vector(160,100))
		self.border_top = Border(vector(-149,95), vector(149,100))
		self.border_bot = Border(vector(-149,-95), vector(149,-100))

		self.paddle1 = Paddle(self.border_top, self.border_bot, vector(-145,0))
		self.paddle2 = Paddle(self.border_top, self.border_bot, vector(145,0))
		self.main_ball = Ball()
		self.main_ball.set_v0()

		self.game_score = Score()

		self.game_state = pong_state(self)
		self.pong_gui = view(self.game_state)
		self.pong_control = controller(self.pong_gui)
		self.input_state = self.pong_control.check_events()

		self.events = [lambda: self.input_state['s_key'],
			lambda: self.input_state['w_key'],
			lambda: self.input_state['up_key'],
			lambda: self.input_state['down_key'],
			lambda: self.input_state['click'] is 'quit',
			lambda: self.input_state['click'] is 'restart',
			self.wall_hit,
			self.left_paddle_hit,
			self.right_paddle_hit,
			self.score_left,
			self.score_right,
			self.check_paddlehits,
			self.init_adv_feat]

		self.responses = [lambda: self.paddle1.move(-1),
			lambda: self.paddle1.move(1),
			lambda: self.paddle2.move(1),
			lambda: self.paddle2.move(-1),
			sys.exit,
			self.restart,
			lambda: self.main_ball.reflect(vector(1,-1)),
			lambda: self.main_ball.reflect(vector(-1,1)),
			lambda: self.main_ball.reflect(vector(-1,1)),
			lambda: self.score(2),
			lambda: self.score(1),
			self.main_ball.turbospeed,
			self.adv_feat_exec]

	# event functions
	def wall_hit(self):
		if (self.main_ball.position.y + self.main_ball.radius) > self.border_top.p1.y:
			return True
		elif (self.main_ball.position.y - self.main_ball.radius) < self.border_bot.p2.y:
			return True

	def left_paddle_hit(self):
		if (self.main_ball.velocity.x < 0):
			paddle_wall = self.paddle1.position.x + self.paddle1.size.x / 2.0
			if -5 < (self.main_ball.position.x - self.main_ball.radius) - paddle_wall < 0:
				if abs(self.main_ball.position.y - self.paddle1.position.y) < (self.paddle1.size.y / 2.0):
					self.state_vars['paddlehit_count'] += 1
					print self.state_vars['paddlehit_count']
					return True

	def right_paddle_hit(self):
		if (self.main_ball.velocity.x > 0):
			paddle_wall = self.paddle2.position.x - self.paddle2.size.x / 2.0
			if 0 < (self.main_ball.position.x + self.main_ball.radius) - paddle_wall < 5:
				if abs(self.main_ball.position.y - self.paddle2.position.y) < (self.paddle2.size.y / 2.0):
					self.state_vars['paddlehit_count'] += 1
					print self.state_vars['paddlehit_count']
					return True

	def score_left(self):
		return (self.main_ball.position.x + self.main_ball.radius) < self.goal1.p2.x

	def score_right(self):
		return (self.main_ball.position.x - self.main_ball.radius) > self.goal2.p1.x

	def check_paddlehits(self):
		if self.state_vars['paddlehit_count'] >= 5:
			self.state_vars['paddlehit_count'] = 0
			return True

	def init_adv_feat(self):
		if self.state_vars['adv_feat_on']:
			self.adv_feat_time += 1
			if self.adv_feat_time > 500:
				self.delete_adv_feat()
				print "Feature timeout."
				self.state_vars['adv_feat_on'] = False
		else:
			if rnd.random() < 0.01:
				self.state_vars['adv_feat_on'] = True
				self.adv_feat_time = 0
				self.state_vars['adv_feat_num'] = rnd.randint(1,8)
				self.create_adv_feat()
				print "Feature " + str(self.state_vars['adv_feat_num']) + " turned on."

		return self.state_vars['adv_feat_on']


	# response functions
	def score(self, player):
		if player == 1:
			self.game_score.score1 += 1
		elif player == 2:
			self.game_score.score2 += 1

		self.reset_pong()

	def restart(self):
		self.game_score.score1 = 0
		self.game_score.score2 = 0
		self.reset_pong()

	def reset_pong(self):
		self.state_vars['paddlehit_count'] = 0

		self.delete_adv_feat()
		self.state_vars['adv_feat_on'] = False

		self.main_ball.set_position(vector(0,0))
		self.main_ball.set_v0()
		self.paddle1.set_position(vector(-145,0))
		self.paddle2.set_position(vector(145,0))

		time.sleep(2)

	def brickhit_resp(self):
		self.brick.break_brick(self.main_ball)
		del self.brick

		self.state_vars['adv_feat_on'] = False

	def adv_feat_exec(self):
		self.adv_events = [lambda: self.portal1.ball_contact(self.main_ball),
			lambda: self.portal2.ball_contact(self.main_ball),
			lambda: self.brick.detect_hit(self.main_ball),
			lambda: self.chaoscloud.ball_contact(self.main_ball),
			lambda: self.creature.detect_hit(self.main_ball),
			lambda: self.creature.check_lingering(),
			lambda: self.state_vars['gravity_on']]

		self.adv_responses = [lambda: self.portal1.teleport(self.main_ball),
			lambda: self.portal2.teleport(self.main_ball),
			self.brickhit_resp,
			lambda: self.chaoscloud.divert(self.main_ball),
			lambda: self.creature.ball_rebound(self.main_ball),
			lambda: self.creature.wander(),
			self.main_ball.gravity]

		if self.state_vars['adv_feat_num'] == 4:
			if self.adv_events[0](): self.adv_responses[0]()
			elif self.adv_events[1](): self.adv_responses[1]()
		elif self.state_vars['adv_feat_num'] == 5:
			if self.adv_events[2](): self.adv_responses[2]()
		elif self.state_vars['adv_feat_num'] == 6:
			if self.adv_events[3](): self.adv_responses[3]()
		elif self.state_vars['adv_feat_num'] == 7:
			if self.adv_events[4](): self.adv_responses[4]()
			elif self.adv_events[5](): self.adv_responses[5]()
		elif self.state_vars['adv_feat_num'] == 8:
			if self.adv_events[6](): self.adv_responses[6]()


	# miscellaneous functions
	def create_adv_feat(self):
		if self.state_vars['adv_feat_num'] == 1:
			self.main_ball.transform_speed(on=True)
		elif self.state_vars['adv_feat_num'] == 2:
			self.paddle1.transform_speed(on=True)
			self.paddle2.transform_speed(on=True)
		elif self.state_vars['adv_feat_num'] == 4:
			port1_rand = vector(rnd.randint(-100,-20), rnd.randint(-80,65))
			port2_rand = vector(rnd.randint(5,85), rnd.randint(-80,65))
			self.portal1 = Portal(port1_rand, port1_rand + vector(15, 30))
			self.portal2 = Portal(port2_rand, port2_rand + vector(15, 30))
			self.portal1.pair_portals(self.portal2),
		elif self.state_vars['adv_feat_num'] == 5:
			brick_rand = vector(rnd.randint(-120,110), rnd.randint(-90,60))
			self.brick = Brick(brick_rand, brick_rand + vector(10,30))
		elif self.state_vars['adv_feat_num'] == 6:
			self.chaoscloud = Chaos(vector(rnd.randint(-120,80), rnd.randint(-70,30)))
		elif self.state_vars['adv_feat_num'] == 7:
			self.creature = Creature(vector(rnd.randint(-120,120), rnd.randint(-70,70)))
		elif self.state_vars['adv_feat_num'] == 8:
			self.state_vars['gravity_on'] = True

	def delete_adv_feat(self):
		if self.state_vars['adv_feat_on']:
			if self.state_vars['adv_feat_num'] == 1:
				self.main_ball.transform_speed(on=False)
			elif self.state_vars['adv_feat_num'] == 2:
				self.paddle1.transform_speed(on=False)
				self.paddle2.transform_speed(on=False)
			elif self.state_vars['adv_feat_num'] == 4:
				for portal in [self.portal1, self.portal2]:
					del portal
			elif self.state_vars['adv_feat_num'] == 5:
				del self.brick
			elif self.state_vars['adv_feat_num'] == 6:
				del self.chaoscloud
			elif self.state_vars['adv_feat_num'] == 7:
				del self.creature
			elif self.state_vars['adv_feat_num'] == 8:
				self.state_vars['gravity_on'] = False

	def step(self):
		self.input_state = self.pong_control.check_events()
		for event, response in zip(self.events, self.responses):
			if event():
				response()
		self.main_ball.move()
		self.game_state.update(self)
		self.pong_gui.update(self.game_state)
		# time.sleep(0.001)
###############################################################################


###############################################################################
# Main Function
###############################################################################

pong_game = pong()

time.sleep(1)

while True:
	pong_game.step()