# class that creates a simple 2D vector and allows simple
# vector operations

class vector():
	def __init__(self, x=0, y=0):
		self.x = x
		self.y = y

	def __add__(self,other):
		return vector(self.x + other.x, self.y + other.y)

	def __sub__(self,other):
		return vector(self.x - other.x, self.y - other.y)

	def __mul__(self,other):
		return vector(self.x * other, self.y * other)

	def __div__(self,other):
		return vector(self.x / other, self.y / other)

	def __abs__(self):
		return vector(abs(self.x), abs(self.y))

	def __repr__(self):
		return "<" + str(self.x) + ", " + str(self.y) + ">"

	def magnitude(self):
		return sqrt(self.x**2 + self.y**2)

	def unit_vec(self):
		return vector(self.x / self.magnitude(), self.y / self.magnitude())