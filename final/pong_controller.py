import simpleGraphics as sg

class controller():
	def __init__(self, GUI):
		self.gui = GUI
		self.win = self.gui.win
		self.quit_button = self.gui.quit_button.repr
		self.restart_button = self.gui.restart_button.repr
		self.events = {'s_key': False, 'w_key': False, 'up_key': False, 'down_key': False, 'click': False}

	def check_events(self):
		location = self.win.checkMouse()
		click_selection = None
		if location is not None:
			if self.quit_button.inRegion(location):
				click_selection = 'quit'
			elif self.restart_button.inRegion(location):
				click_selection = 'restart'

		self.events = {'s_key': self.win.check_s(),
			'w_key': self.win.check_w(),
			'up_key': self.win.check_up(),
			'down_key': self.win.check_down(),
			'click': click_selection}

		return self.events


