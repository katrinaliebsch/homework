import simpleGraphics as sg
from vector_class import vector
import time

class drw_shape():
	def __init__(self, window, position, size, shape, fill_color = 'white', line_color = 'black'):
		if shape is 'circle':
			self.repr = sg.Circle(sg.Point(position.x, position.y), size)
		if shape is 'rectangle':
			p1 = sg.Point(position.x - size.x / 2.0, position.y - size.y / 2.0)
			p2 = sg.Point(position.x + size.x / 2.0, position.y + size.y / 2.0)
			self.repr = sg.Rectangle(p1, p2)

		self.repr.setFill(fill_color)
		self.repr.setOutline(line_color)
		self.repr.draw(window)

	def update(self, position):
		curr_position = self.repr.getCenter()
		dx = position.x - curr_position.x
		dy = position.y - curr_position.y
		self.repr.move(dx, dy)


class area_repr(drw_shape):
	def __init__(self, window, p1, p2, fill_color = 'white', line_color = 'black'):
		position = (p1 + p2) / 2.0
		size = abs(p1 - p2)
		drw_shape.__init__(self, window, position, size, 'rectangle', fill_color, line_color)


class button_repr(drw_shape):
	def __init__(self, window, position, text):
		drw_shape.__init__(self, window, position, vector(30,10), 'rectangle')
		self.button_txt = sg.Text(sg.Point(position.x, position.y), text)
		self.button_txt.draw(window)


class score_display():
	def __init__(self, window, position):
		self.score_drw = sg.Text(sg.Point(position.x, position.y), 0)
		self.score_drw.setSize(20)
		self.score_drw.draw(window)

	def update(self, score):
		self.score_drw.setText(str(score))


class view():
	def __init__(self, data):
		self.win = sg.GraphWin(width=640, height=500)
		self.win.setCoords(-160,-140,160,110)

		self.adv_drawn = False

		self.goal1 = area_repr(self.win, data.goal1_area[0], data.goal1_area[1], fill_color = 'gray', line_color = 'gray')
		self.goal2 = area_repr(self.win, data.goal2_area[0], data.goal2_area[1], fill_color = 'gray', line_color = 'gray')
		self.border_top = area_repr(self.win, data.topborder_area[0], data.topborder_area[1], fill_color = 'black')
		self.border_bot = area_repr(self.win, data.bottomborder_area[0], data.bottomborder_area[1], fill_color = 'black')

		self.score1 = score_display(self.win, vector(-120,-120))
		self.score2 = score_display(self.win, vector(120,-120))
		self.restart_button = button_repr(self.win, vector(-50,-120), "Restart")
		self.quit_button = button_repr(self.win, vector(50, -120), "Quit")

		self.main_ball = drw_shape(self.win, data.ball_position, data.ball_radius, 'circle')
		self.paddle1 = drw_shape(self.win, data.paddle1_position, data.paddle1_size, 'rectangle')
		self.paddle2 = drw_shape(self.win, data.paddle2_position, data.paddle2_size, 'rectangle')

	def update(self, data):
		self.main_ball.update(data.ball_position)
		self.paddle1.update(data.paddle1_position)
		self.paddle2.update(data.paddle2_position)

		self.score1.update(data.scores[0])
		self.score2.update(data.scores[1])

		if data.feature_info[0] & (not self.adv_drawn):
			self.adv_drawn = True
			if data.feature_info[1] == 4:
				self.adv_feature = area_repr(self.win, data.adv_position[0], data.adv_position[1], fill_color = 'blue', line_color = 'blue')
				self.adv_feature2 = area_repr(self.win, data.adv_position[2], data.adv_position[3], fill_color = 'blue', line_color = 'blue')
			elif data.feature_info[1] == 5:
				self.adv_feature = area_repr(self.win, data.adv_position[0], data.adv_position[1], fill_color = 'brown')
			elif data.feature_info[1] == 6:
				self.adv_feature = area_repr(self.win, data.adv_position[0], data.adv_position[1], fill_color = 'white')
			elif data.feature_info[1] == 7:
				self.adv_feature = area_repr(self.win, data.adv_position[0], data.adv_position[1], fill_color = 'green')
			else:
				self.adv_drawn = False
		elif data.feature_info[0] & self.adv_drawn & (data.feature_info[1] == 7):
			self.adv_feature.update((data.adv_position[0] + data.adv_position[1])/2.0)
		elif (not data.feature_info[0]) & self.adv_drawn:
			self.adv_drawn = False	
			self.adv_feature.repr.undraw()
			del self.adv_feature
			if data.feature_info[1] == 4:	
				self.adv_feature2.repr.undraw()
				del self.adv_feature2
