import simpleGraphics as sg
import pong_controller as ctrl
import time

window = sg.GraphWin(width=640, height=500)
control = ctrl.controller(window)

while True:
	results = control.check_events()

	if results['s_key'] or results['w_key'] or results['up_key'] or results['down_key']:
		print results
	if results['click'] is not None:
		print results
